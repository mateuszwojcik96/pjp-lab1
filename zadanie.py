#!/usr/bin/python3

"""Przetwarzanie języka naturalnego, laboratorium 1."""

import collections
import functools
import sqlite3

import matplotlib.pyplot as plt


class Lab1:

    def __init__(self):
        self.KSIĄŻKA = '20-000-mil-podmorskiej-zeglugi.txt'  # TU(3): wpisać nazwę pliku z tekstem książki.
        self.BAZA_DANYCH = sqlite3.connect('polski.sqlite3')
        self.znaki_do_pominiecia = [',', '.']
        self.ilosc = 1
        self.wyrazy = []

    def podaj_wyrazy(self, nazwa_pliku):
        self.wyrazy = []
        with open(nazwa_pliku, 'rt', encoding="utf-8") as plik:
            for czesc in plik.read().split():
                wyraz = czesc.strip(''.join(self.znaki_do_pominiecia))
                if wyraz != '':
                    self.wyrazy.append(wyraz)
                    yield wyraz

    def wypisz_skrajne_znaki_wyrazów(self, nazwa_pliku):
        self.ilosc = 0
        znaki = collections.Counter()
        for wyraz in self.podaj_wyrazy(nazwa_pliku):
            znaki[wyraz[0]] += 1
            znaki[wyraz[-1]] += 1
        for znak, ile_wystąpień in znaki.most_common():
            if not znak.isalnum():
                self.znaki_do_pominiecia.append(znak)
                self.ilosc += 1
                print(znak, ile_wystąpień)

    @functools.lru_cache(maxsize=None)
    def formy_podstawowe(self, wyraz):
        formy = []
        for wiersz in self.BAZA_DANYCH.execute(
                'SELECT base FROM Dictionary WHERE form = ? ORDER BY base',
                [wyraz]):
            formy.append(wiersz[0])
        return formy

    def jednoznaczna_forma_podstawowa(self, wyraz):
        formy = self.formy_podstawowe(wyraz)
        if (len(formy) == 0):
            return wyraz
        elif (len(formy) == 1):
            return formy[0]
        print(min(formy, key=len))
        return min(formy, key=len)
        # TU(7): uzupełnić zgodnie z instrukcją.


def main():
    lab1 = Lab1()
    while (lab1.ilosc > 0):
        lab1.wypisz_skrajne_znaki_wyrazów(lab1.KSIĄŻKA)

    # TU(5): zakomentować dwa powyższe wiersze.
    # TU(5): zaprogramować zliczanie wystąpień poszczególnych
    # wyrazów w kolekcji `wyrazy` typu `collections.Counter`
    # oraz zliczanie sumarycznej liczby wyrazów w zmiennej
    # `długość_tekstu` typu liczbowego.
    # TU(5): wypisać 10 najczęstszych wyrazów i ich częstość.
    długość_tekstu = 0
    wyrazy = collections.Counter()
    for wyraz in lab1.wyrazy:
        wyrazy[wyraz.lower()] += 1
        długość_tekstu += 1

    plt.xscale('log')
    plt.yscale('log')
    # Rysujemy podwójnie logarytmiczny wykres częstości
    # wystąpień wyrazów, od najczęstszego do najrzadszych.
    y = []
    for wyraz, ile_wystąpień in wyrazy.most_common():
        y.append(ile_wystąpień / długość_tekstu)
        # y.append(długość_tekstu/ile_wystąpień)
        print(wyraz + ' ' + str(ile_wystąpień))
    plt.plot(y)
    plt.title(f'Częstość wystąpień {len(wyrazy)} wyrazów\nw książce {lab1.KSIĄŻKA}')
    plt.show()

    # TU(6): skasować powyższy wiersz.

    y = [0]

    formy_podstawowe = collections.Counter()
    ilosc_podst = 0
    for wyraz, ile_wystąpień in wyrazy.most_common():
        for f_podst in lab1.formy_podstawowe(wyraz):
            formy_podstawowe[f_podst] += 1
            ilosc_podst += 1

    for wyraz, ile_wystąpień in formy_podstawowe.most_common():
        y.append(ile_wystąpień / ilosc_podst)


    plt.xscale('log')
    plt.yscale('linear')
    plt.plot(y)
    # y = [0]
    # TU(6): narysować półlogarytmiczny wykres pokrycia tekstu
    # przez wyrazy. N-ty element tablicy `y` ma być równy sumie
    # częstości wyrazów od najczęstszego do N-tego pod względem
    # malejącej częstości.
    plt.title(f'Pokrycie tekstu przez {len(wyrazy)} wyrazów\nw książce {lab1.KSIĄŻKA}')
    plt.show()
    return
    # TU(8): zaprogramować zliczanie wystąpień form podstawowych
    # wyrazów w kolekcji `formy` typu `collections.Counter`
    # i rysowanie wykresów częstości ich wystąpień oraz pokrycia
    # przez nie tekstu.
    plt.title(f'Częstość wystąpień {len(formy)} form podstawowych\nw książce {KSIĄŻKA}')
    plt.title(f'Pokrycie tekstu przez {len(formy)} form podstawowych\nw książce {KSIĄŻKA}')


if __name__ == '__main__':
    main()
